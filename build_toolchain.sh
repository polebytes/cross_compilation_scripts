#!/bin/sh
set -e
TARGET=x86_64-pole-linux-gnu
KERNEL_ARCH=x86
BINUTILS_VERSION=2.42
GCC_VERSION=13.3.0
KERNEL_VERSION=6.10
GLIBC_VERSION=2.39
SRC_DIR=sources
CWD=$(pwd)
BUILD_DIR=${CWD}/out
SYSROOT_DIR=${BUILD_DIR}/${TARGET}/sys-root
SECONDARY_ROOT= #specify the path it will install glibc in it
THREADS=$(nproc --all)
export PATH=$PATH:$BUILD_DIR/bin
mkdir -p ${BUILD_DIR} && mkdir -p ${SYSROOT_DIR}
GLIBC_FHS_PATCH=http://lfs.linux-sysadmin.com/patches/downloads/glibc/glibc-2.39-fhs-1.patch
SRCS=(
	https://mirrors.ocf.berkeley.edu/gnu/gcc/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.gz
	https://mirrors.ocf.berkeley.edu/gnu/binutils/binutils-${BINUTILS_VERSION}.tar.gz
	https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-${KERNEL_VERSION}.tar.xz
	https://mirrors.ocf.berkeley.edu/gnu/glibc/glibc-${GLIBC_VERSION}.tar.gz
)

SUMS=(
	19ae2771e6bc4b803576cc0250d214a67cc7617d
	0332737873c121c43ec3860a9c53647337c38085
	1e9cb1c45e24c18128f4529cc4b93cf3b36d06be
	e3a8c54877bac53ba0defafe9c3e5202789733cf
)

function arrow_echo()
{
	echo "==> $1"
}

function download_gcc_dependencies()
{
	verify_check_sums
	extract_sources
	cd gcc-${GCC_VERSION}
	arrow_echo "downloading gcc dependencies"
	./contrib/download_prerequisites
	cd ${CWD}
}

function verify_check_sums()
{
	counter=0;
	arrow_echo "verifying check sums"
	for links in ${SRCS[@]}; do
		for sums in ${SUMS[@]}; do
			local base_name=$(basename ${links})
			local expected_sum=$(sha1sum ${SRC_DIR}/${base_name} | cut -d " " -f1)
			if [[ ${SUMS[$counter]} == $expected_sum ]]; then
				arrow_echo "checksum for ${base_name} is correct"
			else
				arrow_echo "!checksum for ${base_name} is incorrect"
				exit;
			fi
			let "counter += 1"
			break;
		done
	done
}


function download_sources()
{
	arrow_echo "downloading sources"
	for links in ${SRCS[@]}; do
		local base_name=$(basename $links)
		if [[ -f ${SRC_DIR}/${base_name} ]]; then
			arrow_echo "file alredy exists ${base_name}"
			continue;
		else
			wget ${links} --directory-prefix=$(pwd)/${SRC_DIR}
		fi
	done
	download_gcc_dependencies
}


function extract_sources()
{
	arrow_echo "extracting sources"
	rm -rf binutils-* gcc-*
	for files in ${SRCS[@]}; do
		local file_name=$(basename ${files})
		arrow_echo "extracting sources: ${file_name}"
		tar -xf ${SRC_DIR}/${file_name}
	done
}

function build_binutils()
{
	echo
	arrow_echo "building binutils"
	cd binutils-${BINUTILS_VERSION}
	if [[ -d build ]]; then
		rm -rf build
	fi
	mkdir build && cd build

	../configure \
		--prefix=${BUILD_DIR} \
		--with-sysroot=${SYSROOT_DIR} \
		--target=${TARGET} \
		--disable-nls \
		--enable-gprofng=no \
		--enable-default-hash-style=gnu
	
	make -j${THREADS}
	make install

	cd ${CWD}
}

function build_kernel_headers()
{
	cd linux-${KERNEL_VERSION}

	make ARCH=${KERNEL_ARCH} INSTALL_HDR_PATH=${SYSROOT_DIR}/usr headers_install -j${THREADS}

	cd ${CWD}
}

function build_gcc()
{
	cd gcc-${GCC_VERSION}
	if [[ -d build ]]; then
		rm -rf build
	fi
	#set the default directory name for 64-bit libraries to “lib”
	sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
	mkdir build && cd build

	../configure \
		--target=${TARGET} \
		--prefix=${BUILD_DIR} \
		--with-sysroot=${SYSROOT_DIR} \
		--disable-nls \
		--disable-multilib \
		--disable-libatomic \
		--disable-libgomp \
		--disable-libquadmath \
		--disable-libssp \
		--disable-libvtv \
		--disable-libsanitizer \
		--enable-languages=c,c++

	make -j${THREADS} all-gcc
	make -j${THREADS} install-gcc

	cd ${CWD}
}


function build_glibc_headers()
{
	cd glibc-${GLIBC_VERSION}
	if [[ -d build ]]; then
		rm -rf build
	fi

	wget ${GLIBC_FHS_PATCH} -O glibc_fhs.patch
	patch -Np1 -i glibc_fhs.patch

	mkdir -p build && cd build
	echo "rootsbindir=/usr/sbin" > configparms

	../configure \
		--build=$MACHTYPE \
		--host=$TARGET \
		--target=$TARGET \
		--prefix=/usr \
		--with-sysroot=${SYSROOT_DIR} \
		--with-headers=${SYSROOT_DIR}/usr/include \
		--disable-nscd \
		libc_cv_slibdir=/usr/lib

	make \
		install-bootstrap-headers=yes \
		install_root=${SYSROOT_DIR} \
		install-headers

	#install c startup files
	make -j${THREADS} csu/subdir_lib

	mkdir -p ${SYSROOT_DIR}/usr/lib
	install csu/crt1.o csu/crti.o csu/crtn.o ${SYSROOT_DIR}/usr/lib

	#make a fakefile(null bytes) of libc.so
	${TARGET}-gcc -nostdlib \
	    -nostartfiles \
	    -shared \
	    -x c /dev/null \
	    -o ${SYSROOT_DIR}/usr/lib/libc.so
	touch ${SYSROOT_DIR}/usr/include/gnu/stubs.h

	cd ${CWD}
}

function build_libgcc()
{
	cd gcc-${GCC_VERSION}
	cd build

	make -j${THREADS} all-target-libgcc
	make install-target-libgcc
	cd ${CWD}
}

#install glibc for target system sysroot
function install_glibc_target()
{
	cd glibc-${GLIBC_VERSION}
	cd build

	make -j${THREADS}
	make DESTDIR=${SYSROOT_DIR} install
	make DESTDIR=$(pwd)/_fakeroot install
	if [[ ! -z ${SECONDARY_ROOT} ]]; then
		make install DESTDIR=${SECONDARY_ROOT}
	fi
	
	sed '/RTLDLIST=/s@/usr@@g' -i ${SYSROOT_DIR}/usr/bin/ldd

	cd ${CWD}
}

function build_gcc_final()
{
	cd gcc-${GCC_VERSION}
	cd build
	make -j${THREADS}
	make install

	cd ..
	cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  		`dirname $($TARGET-gcc -print-libgcc-file-name)`/include/limits.h

	cd ${CWD}
}


download_sources
build_binutils
build_kernel_headers
build_gcc
build_glibc_headers
build_libgcc
install_glibc_target
build_gcc_final
